package tn.esprit.pi.epione.resources;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.pi.epione.persistence.DoctolibDoctor;
import tn.esprit.pi.epione.persistence.Doctor;
import tn.esprit.pi.epione.service.AdminService;

@ManagedBean
@SessionScoped
public class AdminBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1974498225740760797L;

	@EJB
	AdminService adminService;
	private String id;
	private String password;
	private String email;
	private boolean isActif;
	private boolean isDoctolib;
	private String name;
	private String img;
	private String address;
	private String city;
	private String path;
	private String speciality;
	private String description;
	private Doctor userConnected;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActif() {
		return isActif;
	}

	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}

	public boolean isDoctolib() {
		return isDoctolib;
	}

	public void setDoctolib(boolean isDoctolib) {
		this.isDoctolib = isDoctolib;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	private double lat;
	private double lng;

	public String activate(int id) {
		System.out.println(id);
		adminService.activated(id);

		return "/Doctor/disactivateDoctor.jsf?faces-redirect=true";
	}

	public String disactivate(int id) {
		System.out.println(id);

		adminService.disactivated(id);

		return "/Doctor/disactivateDoctor.jsf?faces-redirect=true";
	}

	public String remove(int id) {
		adminService.removeDoctor(id);

		return "/Doctor/disactivateDoctor.jsf?faces-redirect=true";
	}

	public List<Doctor> disactivateDoctor() {

		return adminService.getDisactivateDoctors();
	}
	public List<Doctor> doctors() {

		return adminService.getDoctors();
	}

	public List<Doctor> activateDoctors() {

		return adminService.getActivateDoctors();
	}

	public String register() {
		Doctor doc = new Doctor();
		doc.setAddress(address);
		doc.setActif(false);
		doc.setCity(city);
		doc.setDescription(description);
		doc.setDoctolib(false);
		doc.setEmail(email);
		doc.setImg("doctor");
		doc.setLat(lat);
		doc.setLng(lng);
		doc.setName(name);
		doc.setPassword(password);
		doc.setPath(" ");
		doc.setSpeciality(speciality);
			adminService.register(doc);
		return "/login.jsf?faces-redirect=true";
	}

	public String logout(){
		userConnected=null;
		return "/login.jsf?faces-redirect=true";
	}
	
	public String addDoctor(DoctolibDoctor doctor) {
		Doctor doc = new Doctor();
		doc.setAddress(doctor.getAddress());
		doc.setActif(true);
		doc.setCity(doctor.getCity());
		//doc.setDescription(doctor.getDescription());
		doc.setDoctolib(true);
		doc.setEmail(doctor.getName().trim().toLowerCase().replace(" ", ".")+"@epione.com");
		doc.setImg(doctor.getImg());
		doc.setLat(doctor.getLat());
		doc.setLng(doctor.getLng());
		doc.setName(doctor.getName());
		doc.setPassword("admin");
		doc.setPath(doctor.getPath());
		doc.setSpeciality(doctor.getSpeciality());
		adminService.addDoctolib(doc);
		return "/Doctor/doctorsDoctolib.jsf?faces-redirect=true";
	}

	public String login() {
		userConnected = adminService.login(this.email, this.password);
		System.out.println("*********"+userConnected);
		if(userConnected.isActif()==false){
			return "/login.jsf?faces-redirect=true";
		}
		if (userConnected == null) {
			System.out.println("************");
			return "/login.jsf?faces-redirect=true";

		}
		if (email.equals("admin@epione.com")) {

			return "/Doctor/disactivateDoctor.jsf?faces-redirect=true";
		}
		else {
			
			return "/Doctor/listdoctors.jsf?faces-redirect=true";

		}

	}

	public String getDoctor(int id) {

		Doctor doc = adminService.getDoctor(id);
		this.address = doc.getAddress();
		this.name = doc.getName();
		this.email = doc.getEmail();
		this.password = doc.getPassword();
		this.isActif = doc.isActif();
		this.isDoctolib = doc.isDoctolib();
		this.img = doc.getImg();
		this.city = doc.getCity();
		this.path = doc.getPath();
		this.speciality = doc.getAddress();
		this.description = doc.getDescription();

		return "/Doctor/doctorAdmin.jsf?faces-redirect=true";
	}

	public Doctor getUserConnected() {
		return userConnected;
	}

	public void setUserConnected(Doctor userConnected) {
		this.userConnected = userConnected;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
