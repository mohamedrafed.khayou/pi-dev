package tn.esprit.pi.epione.resources;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.pi.epione.persistence.*;
import tn.esprit.pi.epione.service.DoctolibService;

@ManagedBean
@SessionScoped
public class DoctorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6190997677599234036L;

	@EJB
	DoctolibService doctorService;

	private String PaymentMethod;
	private String biography;
	private String Office_Number;
	private String Website;
	private String Doctolib;
	private String OfficeAdress;
	private String Remboursement;
	private String description;
	private String nbreRPPS;
	private String statuts;
	private String nbreInscriptionOrdre;
	private String nbreRCS;
	private Boolean memberAGA;
	private String formeJuridique;
	private String adresseSocialSiege;
	private String socialReason;
	private double lat;
	private double lng;
	private List<String> skills;
	private String name;
	private String img;
	private String address;
	private String city;
	private String path;
	private String specialitie;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSpecialitie() {
		return specialitie;
	}

	public void setSpecialitie(String specialitie) {
		this.specialitie = specialitie;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNbreRPPS() {
		return nbreRPPS;
	}

	public void setNbreRPPS(String nbreRPPS) {
		this.nbreRPPS = nbreRPPS;
	}

	public String getStatuts() {
		return statuts;
	}

	public void setStatuts(String statuts) {
		this.statuts = statuts;
	}

	public String getNbreInscriptionOrdre() {
		return nbreInscriptionOrdre;
	}

	public void setNbreInscriptionOrdre(String nbreInscriptionOrdre) {
		this.nbreInscriptionOrdre = nbreInscriptionOrdre;
	}

	public String getNbreRCS() {
		return nbreRCS;
	}

	public void setNbreRCS(String nbreRCS) {
		this.nbreRCS = nbreRCS;
	}

	public Boolean getMemberAGA() {
		return memberAGA;
	}

	public void setMemberAGA(Boolean memberAGA) {
		this.memberAGA = memberAGA;
	}

	public String getFormeJuridique() {
		return formeJuridique;
	}

	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}

	public String getAdresseSocialSiege() {
		return adresseSocialSiege;
	}

	public void setAdresseSocialSiege(String adresseSocialSiege) {
		this.adresseSocialSiege = adresseSocialSiege;
	}

	public String getSocialReason() {
		return socialReason;
	}

	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public String getDoctorByPath(String path) throws IOException{
		DoctolibDoctor doctor= doctorService.getDoctorByPath(path); 
		this.name=doctor.getName();
		this.address=doctor.getAddress();
		this.city=doctor.getCity();
		this.description=doctor.getDescription();
		this.img=doctor.getImg();
		this.lat=doctor.getLat();
		this.lng=doctor.getLng();
		this.specialitie=doctor.getSpeciality();
		this.skills=doctor.getSkills();
		return "/Doctor/doctor.jsf?faces-redirect=true";
	}
	public List<DoctolibDoctor> getDoctorByAjax(String path) throws IOException{
		List<DoctolibDoctor> doctors= new ArrayList<DoctolibDoctor>();
		DoctolibDoctor doctor= new DoctolibDoctor();
		doctor= doctorService.getDoctorByPath(path);
		doctors.add(doctor);
		return doctors;
	}
	
	public List<Doctolib> getDoctors(String spec,String place ,String page) throws IOException{
		
		return doctorService.getListDoctorsBySpecialityAndLocation( spec,place,page);
	}
	
	public List<DoctolibDoctor> getDoctors2(String spec,String place ,String page) throws IOException{
		List<DoctolibDoctor> list = new ArrayList<>();
		for(Doctolib doc : doctorService.getListDoctorsBySpecialityAndLocation( spec,place,page)){
			DoctolibDoctor doctor= new DoctolibDoctor();
			doctor=doctorService.getDoctorByPath(doc.getPath());
			list.add(doctor);
		}
		return list;
	}
	
	public String search(String search){
		System.out.println(doctorService.search(search));
		return  doctorService.search(search);
	}
	
	
	public String getPaymentMethod() {
		return PaymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	public String getOffice_Number() {
		return Office_Number;
	}
	public void setOffice_Number(String office_Number) {
		Office_Number = office_Number;
	}
	public String getWebsite() {
		return Website;
	}
	public void setWebsite(String website) {
		Website = website;
	}
	public String getDoctolib() {
		return Doctolib;
	}
	public void setDoctolib(String doctolib) {
		Doctolib = doctolib;
	}
	public String getOfficeAdress() {
		return OfficeAdress;
	}
	public void setOfficeAdress(String officeAdress) {
		OfficeAdress = officeAdress;
	}
	public String getRemboursement() {
		return Remboursement;
	}
	public void setRemboursement(String remboursement) {
		Remboursement = remboursement;
	}
}
