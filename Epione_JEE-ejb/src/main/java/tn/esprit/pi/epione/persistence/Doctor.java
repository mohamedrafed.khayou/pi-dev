package tn.esprit.pi.epione.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@Entity
public class Doctor implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String password;
	private String email;
	private boolean isActif;
	private boolean isDoctolib;
	private String name;
	private String img;
	private String address;
	private String city;
	private String path;
	private String speciality;
	private String description;
	private String nbreRPPS;
	private String statuts;
	private String nbreInscriptionOrdre;
	private String nbreRCS;
	private Boolean memberAGA;
	private String formeJuridique;
	private String adresseSocialSiege;
	private String socialReason;
	private double lat;
	private double lng;
	
	
	

	
	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActif() {
		return isActif;
	}

	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNbreRPPS() {
		return nbreRPPS;
	}

	public void setNbreRPPS(String nbreRPPS) {
		this.nbreRPPS = nbreRPPS;
	}

	public String getStatuts() {
		return statuts;
	}

	public void setStatuts(String statuts) {
		this.statuts = statuts;
	}

	public String getNbreInscriptionOrdre() {
		return nbreInscriptionOrdre;
	}

	public void setNbreInscriptionOrdre(String nbreInscriptionOrdre) {
		this.nbreInscriptionOrdre = nbreInscriptionOrdre;
	}

	public String getNbreRCS() {
		return nbreRCS;
	}

	public void setNbreRCS(String nbreRCS) {
		this.nbreRCS = nbreRCS;
	}

	public Boolean getMemberAGA() {
		return memberAGA;
	}

	public void setMemberAGA(Boolean memberAGA) {
		this.memberAGA = memberAGA;
	}

	public String getFormeJuridique() {
		return formeJuridique;
	}

	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}

	public String getAdresseSocialSiege() {
		return adresseSocialSiege;
	}

	public void setAdresseSocialSiege(String adresseSocialSiege) {
		this.adresseSocialSiege = adresseSocialSiege;
	}

	public String getSocialReason() {
		return socialReason;
	}

	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	

	public Doctor() {
		super();
	}

	public boolean isDoctolib() {
		return isDoctolib;
	}

	public void setDoctolib(boolean isDoctolib) {
		this.isDoctolib = isDoctolib;
	}

	@Override
	public String toString() {
		return "Doctor [id=" + id + ", password=" + password + ", email=" + email + ", isActif=" + isActif
				+ ", isDoctolib=" + isDoctolib + ", name=" + name + ", img=" + img + ", address=" + address + ", city="
				+ city + ", path=" + path + ", speciality=" + speciality + ", description=" + description
				+ ", nbreRPPS=" + nbreRPPS + ", statuts=" + statuts + ", nbreInscriptionOrdre=" + nbreInscriptionOrdre
				+ ", nbreRCS=" + nbreRCS + ", memberAGA=" + memberAGA + ", formeJuridique=" + formeJuridique
				+ ", adresseSocialSiege=" + adresseSocialSiege + ", socialReason=" + socialReason + ", lat=" + lat
				+ ", lng=" + lng + "]";
	}
	
 
}