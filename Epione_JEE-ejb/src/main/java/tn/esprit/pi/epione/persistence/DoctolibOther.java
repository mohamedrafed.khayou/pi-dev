package tn.esprit.pi.epione.persistence;

import java.util.ArrayList;
import java.util.List;

public class DoctolibOther extends Doctolib{
	
	private List<Doctolib> lst = new ArrayList<Doctolib>();

	public List<Doctolib> getLst() {
		return lst;
	}

	public void setLst(List<Doctolib> lst) {
		this.lst = lst;
	}
	

}
