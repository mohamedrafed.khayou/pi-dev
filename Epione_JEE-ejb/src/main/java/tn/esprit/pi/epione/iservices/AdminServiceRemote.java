package tn.esprit.pi.epione.iservices;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pi.epione.persistence.DoctolibDoctor;
import tn.esprit.pi.epione.persistence.Doctor;

@Remote
public interface AdminServiceRemote {

	
	public boolean isEmailExist(String email);
	
	public Doctor login(String email,String password);
	public Doctor register(Doctor doctor);
	public boolean	activated(int idDoctor);
	public boolean	disactivated(int idDoctor);
	public boolean removeDoctor(int idDoctor);
	public Doctor	addDoctolib(Doctor doctor);
	public List<Doctor> getActivateDoctors();
	public List<Doctor> getDisactivateDoctors();
	public Doctor getDoctor(int idDoctor);
	public List<Doctor> getDoctors();
	 
}
