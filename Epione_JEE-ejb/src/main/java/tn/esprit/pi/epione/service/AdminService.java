package tn.esprit.pi.epione.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.pi.epione.iservices.AdminServiceRemote;
import tn.esprit.pi.epione.persistence.DoctolibDoctor;
import tn.esprit.pi.epione.persistence.Doctor;

@Stateless
@LocalBean
public class AdminService implements AdminServiceRemote{

	@PersistenceContext(unitName="Epione_JEE-ejb")
	EntityManager em;
	@Override
	public Doctor login(String email, String password) {
		int resultint = (int ) em.createQuery("SELECT count(u) from Doctor u WHERE u.email = :email and u.password = :password and isActif = :true")
				.setParameter("email", email).setParameter("password", password).setParameter("true",true).getFirstResult();
	
			Doctor result = (Doctor) em.createQuery("SELECT u from Doctor u WHERE u.email = :email and u.password = :password ")
					.setParameter("email", email).setParameter("password", password).getSingleResult();
		return result;
		
		
	}

	@Override
	public Doctor register(Doctor doctor) {
		if(this.isEmailExist(doctor.getEmail())){
			return null;
		}
		em.persist(doctor);
		em.flush();
		return null;
	}

	@Override
	public boolean isEmailExist(String email) {
		long result = (long) em.createQuery("SELECT count(u) from Doctor u WHERE u.email = :email")
				.setParameter("email", email).getSingleResult();

		if (result == 0)
			return false;
		else
			return true;
	}

	@Override
	public boolean activated(int idDoctor) {
		Doctor doc = em.find(Doctor.class, idDoctor);
		doc.setActif(true);
		System.out.println(doc);
		em.persist(doc);
		em.flush();
		return true;
	}

	@Override
	public boolean disactivated(int idDoctor) {
		Doctor doc = em.find(Doctor.class, idDoctor);
		doc.setActif(false);
		System.out.println(doc);
		em.persist(doc);
		em.flush();
		return true;
	}

	@Override
	public boolean removeDoctor(int idDoctor) {
		Doctor doc = em.find(Doctor.class, idDoctor);
		em.remove(doc);
		return true;
	}

	@Override
	public Doctor addDoctolib(Doctor doctor) {
		
		em.persist(doctor);
		em.flush();
		return doctor;
	}

	@Override
	public List<Doctor> getActivateDoctors() {
		List<Doctor> resultList = (List<Doctor>) em.createQuery("SELECT u from Doctor u WHERE u.isActif = :true" )
				.setParameter("true", true).getResultList();
		return resultList;
	}

	@Override
	public List<Doctor> getDisactivateDoctors() {
		List<Doctor> resultList = (List<Doctor>) em.createQuery("SELECT u from Doctor u WHERE u.isActif = :false" )
				.setParameter("false", false).getResultList();	
		return resultList;
	}

	@Override
	public Doctor getDoctor(int idDoctor) {

		Doctor doc=em.find(Doctor.class, idDoctor);
		return doc;
	}

	@Override
	public List<Doctor> getDoctors() {
		List<Doctor> resultList = (List<Doctor>) em.createQuery("SELECT u from Doctor u " )
				.getResultList();	
		return resultList;
	}

}
