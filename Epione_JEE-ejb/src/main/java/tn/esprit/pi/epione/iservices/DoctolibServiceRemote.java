package tn.esprit.pi.epione.iservices;

import java.io.IOException;
import java.util.List;

import javax.ejb.Remote;
import javax.json.JsonObject;

import tn.esprit.pi.epione.persistence.Doctolib;
import tn.esprit.pi.epione.persistence.DoctolibDoctor;
import tn.esprit.pi.epione.persistence.DoctolibOther;
import tn.esprit.pi.epione.persistence.Doctor;

@Remote
public interface DoctolibServiceRemote {
	
	
	public List<Doctolib> getListDoctorsBySpecialityAndLocation(String speciality, String location, String page) throws IOException;
	public List<Doctolib> getListDoctorsByNameAndLocation(String name, String location, String page) throws IOException;
	public DoctolibDoctor getDoctorByPath(String path) throws IOException;
	public DoctolibOther getOtherByPath(String path) throws IOException;
	public String search(String search);


}
